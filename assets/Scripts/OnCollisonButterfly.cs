﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollisonButterfly : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "ThirdPersonController_ETHAN")
        {
            Destroy(gameObject, 2);
        }
    }
}
