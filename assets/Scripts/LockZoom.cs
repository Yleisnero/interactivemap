﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockZoom : MonoBehaviour {

    // Use this for initialization
    void Start(){
        OnlineMaps.instance.zoomRange = new OnlineMapsRange(19, 19);
        OnlineMaps.instance.zoom = 19;
    }
}
